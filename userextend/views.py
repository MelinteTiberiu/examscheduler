from django.contrib.auth.models import User
from django.contrib.auth.views import LoginView
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import CreateView

from userextend.forms import UserNewForm


class UserCreateView(CreateView):
    template_name = 'userextend/create_user.html'
    model = User
    form_class = UserNewForm
    success_url = reverse_lazy('login')

    def form_valid(self, form):
        secret_key_input = form.cleaned_data.get('Cod_secret')

        if secret_key_input != "ubblettcj0090":
            form.add_error('Cod_secret', 'Codul secret este gresit!')
            return self.form_invalid(form)

        if form.is_valid():
            new_user = form.save(commit=False)
            new_user.first_name = new_user.first_name.title()
            new_user.last_name = new_user.last_name.upper()
            new_user.save()
            return super().form_valid(form)
        else:
            return super().form_invalid(form)


class CustomLoginView(LoginView):
    template_name = 'registration/login.html'
    success_url = reverse_lazy('home-page')
