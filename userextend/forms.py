from django.contrib.auth.forms import UserCreationForm, PasswordChangeForm
from django.contrib.auth.models import User
from django.forms import TextInput, EmailInput
from django import forms


class UserNewForm(UserCreationForm):
    password1 = forms.CharField(
        label="Parola",
        widget=forms.PasswordInput(attrs={'placeholder': 'Introduceti parola', 'class': 'form-control'})
    )

    password2 = forms.CharField(
        label="Confirmare parola",
        widget=forms.PasswordInput(attrs={'placeholder': 'Reintroduceti parola', 'class': 'form-control'})
    )

    Cod_secret = forms.CharField(
        max_length=20,
        widget=forms.PasswordInput(attrs={'placeholder': 'Introduceti codul secret', 'class': 'form-control'})
    )

    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'username']
        widgets = {
            'first_name': TextInput(attrs={'placeholder': 'Introduceti prenumele', 'class': 'form-control'}),
            'last_name': TextInput(attrs={'placeholder': 'Introduceti numele de familie', 'class': 'form-control'}),
            'username': TextInput(attrs={'placeholder': 'Username-ul va fi folosit pentru logare!', 'class': 'form-control'})
        }
