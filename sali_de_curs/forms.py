from django import forms
from django.forms import TextInput, NumberInput, Textarea

from sali_de_curs.models import Classroom


class ClassroomForm(forms.ModelForm):
    class Meta:
        model = Classroom
        fields = ['name', 'space', 'description', 'corp', 'etaj']
        widgets = {
            'name': TextInput(attrs={'placeholder': 'Introduceti numele salii de curs', 'class': 'form-control'}),
            'space': NumberInput(attrs={'placeholder': 'Introduceti cate locuri are sala de curs (se va imparti automat la 2 pentru examen)', 'class': 'form-control'}),
            'description': Textarea(attrs={'placeholder': 'Introduceti informatii suplimentare (ex: numarul salii)', 'class': 'form-control'}),
            'corp': TextInput(attrs={'placeholder': 'Introduceti corpul salii de curs', 'class': 'form-control'}),
            'etaj': NumberInput(attrs={'placeholder': 'Introduceti etajul salii de curs', 'class': 'form-control'})
        }

        labels = {
            'name': 'Nume Sala',
            'space': 'Numar Locuri',
            'description': 'Descriere',
        }
