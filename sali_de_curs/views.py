from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import CreateView, ListView

from sali_de_curs.filters import ClassroomFilter
from sali_de_curs.forms import ClassroomForm
from sali_de_curs.models import Classroom


class ClassroomCreateView(LoginRequiredMixin, CreateView):
    template_name = 'sali_de_curs/creare_sala.html'
    model = Classroom
    form_class = ClassroomForm
    success_url = reverse_lazy('home-page')


class ClassroomListView(LoginRequiredMixin, ListView):
    template_name = 'sali_de_curs/lista_sali.html'
    model = Classroom
    context_object_name = 'all_classrooms'

    def get_queryset(self):
        return Classroom.objects.filter()

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)
        classrooms = Classroom.objects.filter()
        my_filter = ClassroomFilter(self.request.GET, queryset=classrooms)
        classrooms = my_filter.qs
        data['all_classrooms'] = classrooms
        data['form_filters'] = my_filter.form
        return data
