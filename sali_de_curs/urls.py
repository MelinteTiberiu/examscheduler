from django.urls import path

from sali_de_curs import views

urlpatterns = [
    path('create_classroom/', views.ClassroomCreateView.as_view(), name='create-classroom'),
    path('classroom_list/', views.ClassroomListView.as_view(), name='classroom-list'),

]