from django.db import models


class Classroom(models.Model):
    name = models.CharField(max_length=30)
    space = models.IntegerField()
    description = models.TextField(max_length=300)
    corp = models.CharField(max_length=6)
    etaj = models.IntegerField()

    def __str__(self):
        return self.name
