import django_filters
from sali_de_curs.models import Classroom


class ClassroomFilter(django_filters.FilterSet):
    list_of_spaces = [classroom.space for classroom in Classroom.objects.filter()]
    space_choices = [(space, str(space)) for space in set(list_of_spaces)]

    list_of_corps = [classroom.corp for classroom in Classroom.objects.filter()]
    corp_choices = [(corp, str(corp)) for corp in set(list_of_corps)]

    list_of_etaje = [classroom.etaj for classroom in Classroom.objects.filter()]
    etaj_choices = [(etaj, str(etaj)) for etaj in set(list_of_etaje)]

    name = django_filters.CharFilter(lookup_expr='icontains', label='Numele salii')
    space = django_filters.ChoiceFilter(choices=space_choices)
    corp = django_filters.ChoiceFilter(choices=corp_choices)
    etaj = django_filters.ChoiceFilter(choices=etaj_choices)

    class Meta:
        model = Classroom
        fields = ['name', 'space', 'corp', 'etaj']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.filters['name'].field.widget.attrs.update(
            {'class': 'form-control', 'placeholder': 'Introduceti numele salii'})
        self.filters['space'].field.widget.attrs.update(
            {'class': 'form-select'})
        self.filters['corp'].field.widget.attrs.update(
            {'class': 'form-select'})
        self.filters['etaj'].field.widget.attrs.update(
            {'class': 'form-select'})
