from django.apps import AppConfig


class SaliDeCursConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'sali_de_curs'
