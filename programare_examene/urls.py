from django.urls import path

from programare_examene import views

urlpatterns = [
    path('schedule_classroom/', views.ScheduleExamClassroomCreateView.as_view(), name='schedule-classroom'),
    path('get_available_classrooms/', views.get_available_classrooms, name='get-available-classrooms'),  # for AJAX request
    path('schedule_exam/', views.ScheduleExamCreateView.as_view(), name='schedule-exam'),

]