from django import forms
from django.core.exceptions import ValidationError
from django.core.validators import MinValueValidator, MaxValueValidator
from django.forms import TextInput, NumberInput, Textarea, DateInput, TimeInput, Select, MultipleChoiceField

from programare_examene.models import ScheduleExam, ScheduleExamClassroom
from sali_de_curs.models import Classroom


class ScheduleExamForm(forms.ModelForm):
    class Meta:
        model = ScheduleExam
        fields = '__all__'
        widgets = {
            'nume_prenume': TextInput(attrs={'placeholder': 'Introduceti numele si prenumele dvs.', 'class': 'form-control'}),
            'specializarea': TextInput(attrs={'placeholder': 'Introduceti specializarea', 'class': 'form-control'}),
            'numele_disciplinei': TextInput(attrs={'placeholder': 'Introduceti numele disciplinei', 'class': 'form-control'}),
            'anul': TextInput(attrs={'placeholder': 'Introduceti anul', 'class': 'form-control'}),
            'grupa': TextInput(attrs={'placeholder': 'Introduceti grupa', 'class': 'form-control'}),
            'observatii': TextInput(attrs={'placeholder': 'Introduceti observatii', 'class': 'form-control'}),
            'forma_examinare': TextInput(attrs={'placeholder': 'Introduceti forma de examinare', 'class': 'form-control'}),
            'feedback_rezultate': TextInput(attrs={'placeholder': 'Modalitatea si/sau data de feedback rezultate', 'class': 'form-control'}),
        }
        exam_dates = forms.ModelMultipleChoiceField(
            queryset=ScheduleExamClassroom.objects.all(),
            widget=forms.CheckboxSelectMultiple(attrs={'class': 'form-select'}),
            validators=[
                MinValueValidator(limit_value=2, message='Va rog selectati 2 clase.'),
                MaxValueValidator(limit_value=2, message='Va rog nu selectati mai mult de 2 clase.')
            ]
        )

        labels = {
            'exam_dates': 'Clase programate'
        }


class SalaExamenWidget(forms.Select):
    def get_context(self, name, value, attrs):
        context = super().get_context(name, value, attrs)
        widget = context['widget']

        # Add the custom attributes within the 'attrs' dictionary.
        widget['attrs']['data-data-url'] = '/get_available_classrooms/'
        widget['attrs']['data-depended-fields'] = 'data ora_incepere durata nr_studenti'

        # Set the 'class' attribute for styling.
        widget['attrs']['class'] = 'form-control'

        return context


class ScheduleExamClassroomForm(forms.ModelForm):
    NR_STUDENTS = [(space, space) for space in Classroom.objects.values_list('space', flat=True).distinct()]
    nr_studenti = forms.ChoiceField(choices=NR_STUDENTS, widget=forms.Select(attrs={'class': 'form-control'}))

    sala_examen = forms.ModelChoiceField(
        queryset=Classroom.objects.all(),
        widget=SalaExamenWidget,
    )

    class Meta:
        model = ScheduleExamClassroom
        fields = ['data', 'ora_incepere', 'durata', 'nr_studenti', 'sala_examen']
        widgets = {
            'data': DateInput(attrs={'class': 'form-control', 'type': 'date'}),
            'ora_incepere': TimeInput(attrs={'class': 'form-control', 'type': 'time'}),
            'durata': TimeInput(attrs={'class': 'form-control', 'type': 'time'})
        }
