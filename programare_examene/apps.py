from django.apps import AppConfig


class ProgramareExameneConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'programare_examene'
