from datetime import datetime, timedelta

from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User
from django.db.models import Q
from django.http import JsonResponse
from django.urls import reverse_lazy
from django.views.generic import CreateView

from programare_examene.forms import ScheduleExamClassroomForm, ScheduleExamForm
from programare_examene.models import ScheduleExamClassroom, ScheduleExam
from sali_de_curs.models import Classroom


class ScheduleExamCreateView(LoginRequiredMixin, CreateView):
    model = ScheduleExam
    form_class = ScheduleExamForm
    template_name = 'programare_examene/creare_programare.html'
    success_url = reverse_lazy('home-page')

    def form_valid(self, form):
        selected_exam_dates = form.cleaned_data['exam_dates']
        available_exam_dates = ScheduleExamClassroom.objects.filter(
            user=self.request.user
        ).exclude(id__in=[obj.id for obj in selected_exam_dates])
        form.fields['exam_dates'].queryset = available_exam_dates
        form.instance.user = self.request.user
        return super().form_valid(form)


class ScheduleExamClassroomCreateView(LoginRequiredMixin, CreateView):
    model = ScheduleExamClassroom
    form_class = ScheduleExamClassroomForm
    template_name = 'programare_examene/creare_programare_clasa.html'
    success_url = reverse_lazy('home-page')

    def form_valid(self, form):
        if form.is_valid():
            schedule_exam = form.save(commit=False)
            schedule_exam.user = self.request.user
            schedule_exam.save()
            return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        # Disable the classroom fields until the user has selected the dates and hours.
        context['form'].fields['sala_examen'].disabled = True

        return context


def get_available_classrooms(request):
    # Get the selected values from the GET request parameters.
    selected_data = request.GET.get('data')
    selected_ora_incepere = request.GET.get('ora_incepere')
    selected_durata = request.GET.get('durata')
    selected_nr_studenti = request.GET.get('nr_studenti')

    # Parse selected_data and selected_ora_incepere to datetime objects.
    selected_data = datetime.strptime(selected_data, '%Y-%m-%d')
    selected_ora_incepere = datetime.strptime(selected_ora_incepere, '%H:%M')

    hours, minutes = map(int, selected_durata.split(':'))
    selected_durata = timedelta(hours=hours, minutes=minutes)

    end_time = selected_ora_incepere + selected_durata

    if selected_durata.total_seconds() >= 12 * 3600:
        query_part1 = Q(ora_incepere__gte=end_time)

        query_part2 = Q(ora_incepere__lt=selected_ora_incepere)

        already_booked_filter = query_part1 | query_part2
    else:
        already_booked_filter = Q(
            Q(ora_incepere__lte=selected_ora_incepere, ora_incepere__gt=end_time) |
            Q(ora_incepere__lt=end_time, ora_incepere__gte=selected_ora_incepere - timedelta(hours=1))
        )

    booked_classrooms = ScheduleExamClassroom.objects.filter(
        data=selected_data,
        sala_examen__space=selected_nr_studenti
    ).filter(already_booked_filter)

    booked_classrooms_ids = booked_classrooms.values_list('sala_examen_id', flat=True)

    available_classrooms = Classroom.objects.filter(
        space=selected_nr_studenti
    ).exclude(id__in=booked_classrooms_ids)
    # Convert the filtered classrooms to a JSON-friendly format.
    classrooms_data = [{
        'id': classroom.id,
        'name': classroom.name,
    } for classroom in available_classrooms]

    print(f"This is the classrooms_data: {classrooms_data}")
    # Return the JSON response with the available classrooms.
    return JsonResponse({'classrooms': classrooms_data})
