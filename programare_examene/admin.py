from django.contrib import admin

from programare_examene.models import ScheduleExamClassroom, ScheduleExam

admin.site.register(ScheduleExamClassroom)
admin.site.register(ScheduleExam)

