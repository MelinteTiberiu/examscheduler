from datetime import time

from django.contrib.auth.models import User
from django.db import models

from sali_de_curs.models import Classroom


class ScheduleExam(models.Model):
    nume_prenume = models.CharField(max_length=35)
    specializarea = models.CharField(max_length=50)
    numele_disciplinei = models.CharField(max_length=50)
    anul = models.CharField(max_length=6)
    grupa = models.CharField(max_length=30, null=True, blank=True)
    observatii = models.CharField(max_length=20, null=True, blank=True)
    forma_examinare = models.CharField(max_length=5, null=True, blank=True)

    exam_dates = models.ManyToManyField('ScheduleExamClassroom')
    feedback_rezultate = models.CharField(max_length=60, null=True, blank=True)


class ScheduleExamClassroom(models.Model):
    data = models.DateField()
    ora_incepere = models.TimeField()
    durata = models.DurationField()
    nr_studenti = models.PositiveIntegerField()
    sala_examen = models.ForeignKey(Classroom, on_delete=models.SET_NULL, null=True, blank=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        formatted_ora_incepere = self.ora_incepere.strftime('%H:%M')
        formatted_date = self.data.strftime('%d/%m')
        return f'Data: {formatted_date}, Ora: {formatted_ora_incepere}, Sala: {self.sala_examen}'
