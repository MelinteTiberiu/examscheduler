from django.shortcuts import render
from django.views.generic import TemplateView


class HomeTemplateView(TemplateView):
    template_name = 'home/homepage.html'  # specificam calea care pagina HTML (ATENTIE!!! paginile html se afla in folderul TEMPLATES)
